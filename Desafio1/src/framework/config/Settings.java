package framework.config;

import framework.base.Browser;
import framework.utilities.ExcelUtil;
import framework.utilities.LogUtil;

public class Settings {

    public static String LogPath;
    public static LogUtil Logs;
    public static Browser.BrowserType BrowserType;
    public static ExcelUtil ExcelSheet;
    public static String ExcelSheetPath;
    public static String CsvPath;
    public static String ScreenshotPath;
    public static String ReportConfigPath;

    public static String UrlPocCodigoBarrasNaoLogado;
    public static String UrlPocLogado;
    public static String UrlMinhaOiLogado;
    public static String UrlPocContaOnlineNaoLogado;

}
