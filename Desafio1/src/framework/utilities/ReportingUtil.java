package framework.utilities;

import com.aventstack.extentreports.ExtentReports;
import framework.config.Settings;

import java.io.UnsupportedEncodingException;

public class ReportingUtil {

    private ExtentReports extentReports;

    public void SetGherkinDialect() throws UnsupportedEncodingException {
        extentReports.setGherkinDialect("pt");
    }

    public void FlushExtentReport() {
        extentReports.flush();
    }



    /*
    public static ExtentTest scenarioDef;

    public static ExtentTest features;

    String fileName = Settings.ReportConfigPath + "extentreport.html";

    public void ExtentReport(){
        extent = new ExtentReports();

        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(fileName);
        htmlReporter.config().setTheme(Theme.DARK);
        htmlReporter.config().setDocumentTitle("Test report for Conta Online");
        htmlReporter.config().setEncoding("utf-8");
        htmlReporter.config().setReportName("Test report");

        extent.attachReporter(htmlReporter);
    }

    public void ExtentReportScreenshot() throws IOException {
        var scr = ((TakesScreenshot) DriverContext.Driver).getScreenshotAs(OutputType.FILE);
        Files.copy(scr.toPath(), new File(Settings.ReportConfigPath + "screenshot.png").toPath());
        //scenarioDef.fail
    }

    public void FlushReport(){
        extent.flush();
    }


    /*
    public static ExtentTest extentTest;
    public static String extentReportFile;

    public static ExtentReports extent;
    static String nomeProjeto = "ECommerce";


    public static void gerarReport(String testeAtual) throws Exception {

        File extentReportFile1 = new File(getcaminhoReport());
        Date data = new Date();
        SimpleDateFormat formatdata = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        String dataFormatada = formatdata.format(data);

        if (!extentReportFile1.exists()) {
            extentReportFile1.mkdirs();

        }
        extentReportFile = getcaminhoReport()+"Report_" +nomeProjeto +"_"+ testeAtual +"_"+ dataFormatada + ".html";
        extent = new ExtentReports(extentReportFile, false);
        extent.loadConfig(new File( System.getProperty("user.dir") + "\\src\\main\\java\\Report\\extent-config.xml"));

    }

    public static void gerarreportErro(String cenarioatual, String detalhesdafalha) throws Exception {

        try {
            String screenshotImagensErro = geraEvidenciaReport(cenarioatual, driver);
            extentTest.log(LogStatus.INFO, "Error Snapshot : " + extentTest.addScreenCapture(screenshotImagensErro));
            extentTest.log(LogStatus.FAIL, "Falhou - " + detalhesdafalha);
        }catch(Exception e){

        }
    }

    public static void geraEvidenciaParaReport(String cenarioatual) throws Exception {

        try {
            String screenshotImagens = geraEvidenciaReportSucesso(cenarioatual);
            extentTest.log(LogStatus.PASS, cenarioatual + extentTest.addScreenCapture(screenshotImagens));
        }catch(Exception e){

        }
    }

    public static void escreveReport(){
        extent.endTest(extentTest); // Finaliza o teste corrente e prepara a criação do HTML

    }

    public static void fechaReport(){

        extent.flush(); // Escreve ou atualiza as informações do teste no report.
        extent.close(); // chama o close() de to do o teste da sua sessão e limpa os recursos.

    }*/
}
