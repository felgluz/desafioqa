package test.runner;

import com.aventstack.extentreports.ExtentReports;

import com.aventstack.extentreports.ExtentTest;
import cucumber.api.CucumberOptions;

import cucumber.api.Scenario;
import cucumber.api.junit.Cucumber;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.TakesScreenshot;

import java.io.UnsupportedEncodingException;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        features = {"src/test/test/features/"},
        dryRun = false,             //Verifica cada step dentro da feature
        strict = false,             //bloqueia execução caso cucumber encontre steps não definidos
        glue = {"test.steps"},
        //tags = {"@codigo-de-barras-logado", "@formas-de-pagamento-logado", "@sidebar-logado"},
        //tags = {"@formas-de-pagamento-logado"},
        tags = {"@formas-pagamento-nao-logado"},
        //tags = {"@codigo-de-barras-logado"},
        //tags = {"@sidebar-logado"},
        //tags = {"@conta-online"},
        //tags = {"@wip"},
        plugin = {"pretty", "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:" +
                "src/test/test/data/"
        }
)

public class TestRunner {

    @BeforeClass()
    public static void ExtentReportCreation() throws UnsupportedEncodingException {
        ExtentReports extentReports = new ExtentReports();
        extentReports.flush();
        extentReports.setGherkinDialect("pt");

    }

    @AfterClass()
    public static void Screenshot(){

    }
}